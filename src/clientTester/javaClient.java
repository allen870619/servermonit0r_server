package clientTester;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.Socket;
import org.json.*;

public class javaClient {
    public static void main(String[] args) {
        CClient client = new CClient();
        client.run();
    }
}

class CClient implements Runnable {

    @Override
    public void run() {
        try {
            InetAddress serverIp = InetAddress.getByName("192.168.1.110");
            int serverPort = 9943;
            Socket clientSocket = new Socket(serverIp, serverPort);

            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));
            System.out.println("run?");
            BufferedReader br = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

            while (true) {

                while (clientSocket.isConnected()) {

                    StringBuilder sb = new StringBuilder();
                    String recv = br.readLine();
                    if (recv == null) {
                        break;
                    }
                    sb.append(recv).append(System.lineSeparator());
                    String content = sb.toString();
                 // as example, you can see the content in console output
                    JSONObject root = new JSONObject(content);
                    System.out.println(root.toString());
                    System.out.println(root.getJSONObject("cpu").getInt("cpuUsage"));

                }
                System.out.println("server down?");
                break;
            }
            clientSocket.close();

        } catch (IOException e) {
            System.out.println("error");
        }

    }

}
