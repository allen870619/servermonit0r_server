import psutil
import datetime
import time
import math

preUp = 0
preDw = 0

def getSensorData():
    global preDw,preUp
    # cpu usage(single)
    print("CPU Usage:"+str(psutil.cpu_percent(interval=1, percpu=False)))
    # cpu usage free
    print("CPU Free:"+str(psutil.cpu_times_percent().idle))

    # cpu temp
    try:
        print("Cpu temp:"+str(psutil.sensors_temperatures()['coretemp'][0].current))
    except AttributeError:
        print("Cpu temp: N/A")

    # cpu freq
    print("CPU Freq:"+str(psutil.cpu_freq(percpu=False).current)+"Mhz")
    print("CPU Freq:"+str(psutil.cpu_freq(percpu=False).min)+"Mhz")
    print("CPU Freq:"+str(psutil.cpu_freq(percpu=False).max)+"Mhz")

    # mem
    # total
    print("Mem Total:"+str(psutil.virtual_memory().total/1024/1024)+" MB")
    # usage
    print("Mem Usage:"+str(psutil.virtual_memory().percent))

    
    # disk
    # print(psutil.disk_partitions(all=False))
    # print(psutil.disk_usage("/"))

    # network
    curUp = psutil.net_io_counters().bytes_sent/1024/1024*8
    curDw = psutil.net_io_counters().bytes_recv/1024/1024*8
    
    print("Net UP:"+str('%.2f'%(curUp - preUp)) +
          "Mbps Down:"+str('%.2f'%(curDw - preDw))+"Mbps")
    preUp = curUp
    preDw = curDw

     # total logical cpu
    print("CPU core num:"+str(psutil.cpu_count(logical=False)) +
          " Cores, "+str(psutil.cpu_count(logical=True))+" Threads")
    
    # boot time(s)
    now = time.time()
    uptime = math.floor(now - psutil.boot_time())
    print("boot times:"+str(datetime.timedelta(seconds=uptime)))


while(True):
    print("----------")
    getSensorData()
