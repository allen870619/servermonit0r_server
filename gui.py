import tkinter as tk


def initWindow():
    window = tk.Tk()
    window.title("ServerMonit0r Server")
    window.geometry("400x360")
    window.maxsize(480, 320)
    window.resizable(0, 0)
    return window


def testRed(obj):
    obj["bg"] = "red"


window = initWindow()

# connection state part
connectFrame = tk.Frame(window)
labelCnt = tk.Label(connectFrame, text="Connection List")
labelCnt.pack(pady=(16, 0))
listDevice = tk.Listbox(connectFrame, width="24", height="16")
for i in range(50):
    listDevice.insert(i, "192.168.1.1"+str(i))
listDevice.pack(pady="16")
connectFrame.pack(side="left", fill="both", padx=(16,8))

# setting part
settingFrame = tk.Frame(window)
labelIP = tk.Label(settingFrame, text="IP")
labelIP.pack()
labelInet = tk.Label(settingFrame, text="XXX.XXX.XXX.XXX")
labelInet.pack(side="left")
settingFrame.pack(side="left", fill="both",  padx=(8,16))


window.mainloop()
